#!/bin/bash

_prepare() {
  apk --no-cache add build-base libffi-dev ruby-dev tar git curl
  gem install fpm --no-ri --no-rdoc
}

_after-install-script() {
local STYLE=$1
local COLOR=$2
cat > update-icons << EOS
#!/bin/bash

numix-folders --cli << EOF
$STYLE
$COLOR
EOF
EOS
}

_fpm() {
local STYLE=$1
local COLOR=$2
case "$STYLE" in
  "1")
    STYLE="Original"
  ;;
  "2")
    STYLE="Plain"
  ;;
  "3")
    STYLE="Tilted"
  ;;
  "4")
    STYLE="Circle Redesign"
  ;;
  "5")
    STYLE="Curvy"
  ;;
  "6")
    STYLE="Current"
  ;;
esac
  fpm -s empty -t deb --name "numix-folders-$STYLE-$COLOR" --version "$CI_BUILD_TAG" --vendor "Numix Project Ltd." --maintainer "morph027 <morphsen@gmx.com>" --description "Numix Folders - $STYLE $COLOR" --url "https://github.com/numixproject/numix-folders" --depends "numix-icon-theme | numix-icon-theme-circle" --depends "numix-folders" --after-install update-icons --before-remove revert-icons
}

which fpm || _prepare

STYLES=()
STYLES+=( "1" )
STYLES+=( "2" )
STYLES+=( "3" )
STYLES+=( "4" )
STYLES+=( "5" )
STYLES+=( "6" )

COLORS=()
COLORS+=( "blue" )
COLORS+=( "brown" )
COLORS+=( "green" )
COLORS+=( "grey" )
COLORS+=( "orange" )
COLORS+=( "pink" )
COLORS+=( "purple" )
COLORS+=( "red" )
COLORS+=( "yellow" )

for STYLE in ${STYLES[@]}
do
  for COLOR in ${COLORS[@]}
  do
    _after-install-script "$STYLE" "$COLOR"
    _fpm "$STYLE" "$COLOR"
  done
done
