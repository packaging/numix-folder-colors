#!/bin/bash

if [ ! -z $APTLY_CLI_URL ]; then

  VARS=( REPO_NAME REPO_DIST CI_PROJECT_ID )
  . <(curl -fsSL https://gitlab.com/morph027/gitlab-ci-helpers/raw/master/check-vars.sh)

  curl -fsSL -X POST \
  -F token=$APTLY_CLI_PRIVATE_TOKEN \
  -F ref=master \
  -F "variables[REPO_NAME]=$REPO_OS" \
  -F "variables[REPO_DIST]=$REPO_DIST" \
  -F "variables[PROJECT]=$CI_PROJECT_ID" \
  -F "variables[STAGE]=create_package" \
  -F "variables[REF]=$CI_BUILD_REF_NAME" \
  $APTLY_CLI_URL

fi
