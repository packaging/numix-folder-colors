# Numix folder colors

![](https://gitlab.com/packaging/numix-folder-colors/raw/master/numix-folder-colors.png)

Just a bunch of simple meta packages which depends on [numix-folders](https://github.com/numixproject/numix-folders) ([PPA](https://launchpad.net/~numix/+archive/ubuntu/ppa)) to create colored numix icons. Useful for orchestration (puppet/salt/ansible/preseed/whatever).

You can find packages in [Pipelines](https://gitlab.com/packaging/numix-folder-colors/pipelines).

## Internals

It just depends on the package ```numix-folders``` being installed and then executes the neccessary commands after being installed:

```bash
numix-folders --cli << EOF
$STYLE
$COLOR
EOF
```
